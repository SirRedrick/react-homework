export interface IMessage {
  id: string;
  userId: string;
  avatar: string | undefined;
  user: string;
  text: string | undefined;
  createdAt: string;
  editedAt: string;
  isLiked: boolean;
}

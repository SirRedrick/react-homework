import * as React from 'react';
import { v4 as uuidv4 } from 'uuid';
import { Container } from '@material-ui/core';
import { DateTime } from 'luxon';
import Header from './Header';
import MessageList from './MessageList';
import MessageInput from './MessageInput';
import ChatForm from './ChatForm';
import { IMessage } from '../../types/chat';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';

const styles = createStyles({
  root: {
    height: '100vh'
  },
  container: {
    height: '100%',
    display: 'grid',
    gridTemplateRows: '64px calc(100% - 164px) 100px'
  },
  messageList: {
    overflowY: 'auto'
  }
});

interface Props extends WithStyles<typeof styles> {
  url: string;
}

type State = {
  userName: string;
  userId: string;
  name: string;
  messages: IMessage[];
  userCount: number;
  lastTime: string | null;
  isLoaded: boolean;
  edited: string | null;
  modalOpened: boolean;
};

class Chat extends React.Component<Props, State> {
  state: State = {
    userName: 'name',
    userId: uuidv4(),
    name: 'chat',
    messages: [],
    userCount: 0,
    lastTime: null,
    isLoaded: false,
    edited: null,
    modalOpened: false
  };

  constructor(props: Props) {
    super(props);

    this.onConfirm = this.onConfirm.bind(this);
    this.onLike = this.onLike.bind(this);
    this.onAdd = this.onAdd.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.edit = this.edit.bind(this);
    this.cancelEdit = this.cancelEdit.bind(this);
  }

  onConfirm(userName: string, chatName: string) {
    this.setState(prevState => ({ ...prevState, name: chatName, modalOpened: false, userName }));
  }

  componentDidMount() {
    fetch(this.props.url)
      .then(res => res.json())
      .then(result => {
        const dates = result.map((message: IMessage) =>
          DateTime.fromISO(message.createdAt, { zone: 'UTC' })
        );
        const lastTime = DateTime.max(...dates)?.toFormat('dd.LL.yyyy HH:mm');

        const userIds = result.map((message: IMessage) => message.userId);
        const userCount = new Set(userIds).size;

        this.setState({
          ...this.state,
          messages: result.map((message: IMessage) => ({ ...message, isLiked: false })),
          isLoaded: true,
          lastTime,
          userCount
        });
      });
  }

  onAdd(text: string | undefined) {
    const message: IMessage = {
      id: uuidv4(),
      userId: this.state.userId,
      avatar: undefined,
      user: this.state.userName,
      text,
      createdAt: DateTime.now().toISO()!,
      editedAt: '',
      isLiked: false
    };

    const messages = this.state.messages.concat(message);

    const dates = messages.map((message: IMessage) =>
      DateTime.fromISO(message.createdAt, { zone: 'UTC' })
    );
    const lastTime = DateTime.max(...dates)?.toFormat('dd.LL.yyyy HH:mm');

    const userIds = messages.map((message: IMessage) => message.userId);
    const userCount = new Set(userIds).size;

    this.setState({
      ...this.state,
      messages,
      userCount,
      lastTime
    });
  }

  onLike(id: string) {
    const messages = this.state.messages.map((message: IMessage) =>
      message.id === id ? { ...message, isLiked: !message.isLiked } : message
    );

    this.setState({ ...this.state, messages });
  }

  onDelete(id: string) {
    const messages = this.state.messages.filter((message: IMessage) => message.id !== id);

    const userIds = messages.map((message: IMessage) => message.userId);
    const userCount = new Set(userIds).size;
    this.setState({
      ...this.state,
      messages,
      userCount
    });
  }

  onEdit(id: string) {
    this.setState({ ...this.state, edited: id });
  }

  edit(text: string | undefined) {
    this.setState({
      ...this.state,
      edited: null,
      messages: this.state.messages.map((message: IMessage) =>
        message.id === this.state.edited
          ? { ...message, text, editedAt: DateTime.now().toISO() }
          : message
      )
    });
  }

  cancelEdit() {
    this.setState({ ...this.state, edited: null });
  }

  render() {
    const { userId, name, messages, userCount, lastTime, isLoaded, edited, modalOpened } =
      this.state;

    const { classes } = this.props;

    return (
      <Container maxWidth="lg" classes={{ root: classes.root }} className="chat">
        <div className={classes.container}>
          <Header
            name={name}
            userCount={userCount}
            messagesCount={messages.length}
            lastTime={lastTime}
          />
          <MessageList
            userId={userId}
            messages={messages}
            isLoaded={isLoaded}
            onLike={this.onLike}
            onEdit={this.onEdit}
            onDelete={this.onDelete}
          />
          <MessageInput
            onAdd={this.onAdd}
            edited={edited}
            text={messages.find((message: IMessage) => message.id === edited)?.text}
            edit={this.edit}
            cancelEdit={this.cancelEdit}
          />
          <ChatForm open={modalOpened} onConfirm={this.onConfirm} />
        </div>
      </Container>
    );
  }
}

export default withStyles(styles)(Chat);

import * as React from 'react';
import {
  ListItem,
  ListItemAvatar,
  ListItemText,
  Avatar,
  IconButton,
  Grid
} from '@material-ui/core';
import { Favorite as FavoriteIcon, FavoriteBorder as FavoriteBorderIcon } from '@material-ui/icons';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';

const styles = createStyles({
  root: {
    width: 'max-content'
  },
  messageText: {
    flexGrow: 0
  }
});

interface Props extends WithStyles<typeof styles> {
  name: string;
  text: string | undefined;
  time: string;
  avatar: string | undefined;
  isLiked: boolean;
  onLike: () => void;
}

class Message extends React.Component<Props> {
  render() {
    const { name, text, time, avatar, isLiked, classes, onLike } = this.props;

    return (
      <ListItem className="message" classes={{ root: classes.root }} alignItems="center">
        <ListItemAvatar>
          <Avatar src={avatar} alt="" className="message-user-avatar" />
        </ListItemAvatar>
        <ListItemText
          className={classes.messageText}
          primary={
            <React.Fragment>
              <span className="message-user-name">{name}</span>
              <small className="message-time">{time}</small>
            </React.Fragment>
          }
          secondary={
            <Grid container>
              <Grid item xs={12}>
                <span className="message-text">{text}</span>
              </Grid>
              <Grid item xs={12}>
                {' '}
                <IconButton
                  color="primary"
                  size="small"
                  className={isLiked ? 'message-liked' : 'message-like'}
                  onClick={onLike}
                >
                  {isLiked ? (
                    <FavoriteIcon fontSize="small" />
                  ) : (
                    <FavoriteBorderIcon fontSize="small" />
                  )}
                </IconButton>
              </Grid>
            </Grid>
          }
        />
      </ListItem>
    );
  }
}

export default withStyles(styles)(Message);

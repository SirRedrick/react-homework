import * as React from 'react';
import { List, ListSubheader, Divider, Typography } from '@material-ui/core';
import { DateTime } from 'luxon';
import Message from './Message';
import OwnMessage from './OwnMessage';
import Preloader from '../common/Preloader';
import { IMessage } from '../../types/chat';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';

const styles = createStyles({
  listWrapper: {
    overflowY: 'auto'
  },
  ul: {
    padding: 0
  }
});

interface Props extends WithStyles<typeof styles> {
  userId: string;
  messages: IMessage[];
  isLoaded: boolean;
  onLike: (id: string) => void;
  onEdit: (id: string) => void;
  onDelete: (id: string) => void;
}

class MessageList extends React.Component<Props> {
  render() {
    const { userId, messages, isLoaded, classes, onLike, onEdit, onDelete } = this.props;
    const dividedMessages = messages
      .sort((a, b) => {
        return DateTime.fromISO(b.createdAt).toMillis() - DateTime.fromISO(a.createdAt).toMillis();
      })
      .reduce<Record<string, IMessage[]>>((acc, message: IMessage) => {
        const messageTime = DateTime.fromISO(message.createdAt);
        const date = messageTime.toFormat('cccc, d LLLL');

        const diff = Math.floor(DateTime.now().diff(messageTime, ['days']).days);
        switch (diff) {
          case 0:
            acc['Today'] = acc['Today'] ? acc['Today'].concat(message) : [message];
            break;
          case 1:
            acc['Yesterday'] = acc['Yesterday'] ? acc['Yesterday'].concat(message) : [message];
            break;
          default:
            acc[date] = acc[date] ? acc[date].concat(message) : [message];
        }

        return acc;
      }, {});

    return (
      <div className={classes.listWrapper}>
        <List className="message-list" subheader={<li />}>
          {isLoaded ? (
            Object.entries(dividedMessages).map(([key, value]) => {
              return (
                <li key={key}>
                  <ul className={classes.ul}>
                    <ListSubheader>
                      <Divider className="messages-divider" />
                      <Typography>{key}</Typography>
                    </ListSubheader>
                    {value.map((message: IMessage) => {
                      const { user, text, createdAt, avatar, isLiked } = message;
                      const time = DateTime.fromISO(createdAt, { zone: 'UTC' });

                      const formattedTime = time.toFormat('HH:mm');
                      return userId === message.userId ? (
                        <OwnMessage
                          key={message.id}
                          text={text}
                          time={formattedTime}
                          onEdit={() => onEdit(message.id)}
                          onDelete={() => onDelete(message.id)}
                        />
                      ) : (
                        <Message
                          key={message.id}
                          name={user}
                          text={text}
                          time={formattedTime}
                          avatar={avatar}
                          isLiked={isLiked}
                          onLike={() => onLike(message.id)}
                        />
                      );
                    })}
                  </ul>
                </li>
              );
            })
          ) : (
            <Preloader />
          )}
        </List>
      </div>
    );
  }
}

export default withStyles(styles)(MessageList);

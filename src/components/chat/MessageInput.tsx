import * as React from 'react';
import { TextField, Button, ButtonGroup } from '@material-ui/core';

type Props = {
  onAdd: (text: string | undefined) => void;
  edited: string | null;
  text: string | undefined;
  edit: (text: string | undefined) => void;
  cancelEdit: () => void;
};

type State = {
  text: string | undefined;
};
class MessageInput extends React.PureComponent<Props, State> {
  state: State = { text: this.props.edited ? '' : this.props.text };

  onChange(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void {
    this.setState({ text: e.currentTarget.value });
  }

  render() {
    return (
      <div className="message-input">
        <TextField
          className="message-input-text"
          type="text"
          value={this.state.text}
          onChange={e => this.onChange(e)}
        />
        {this.props.edited ? (
          <ButtonGroup>
            <Button
              className="message-input-button"
              onClick={() => this.props.edit(this.state.text)}
            >
              Send
            </Button>
            <Button onClick={this.props.cancelEdit}>Cancel</Button>
          </ButtonGroup>
        ) : (
          <Button
            className="message-input-button"
            onClick={() => this.props.onAdd(this.state.text)}
          >
            Send
          </Button>
        )}
      </div>
    );
  }
}

export default MessageInput;

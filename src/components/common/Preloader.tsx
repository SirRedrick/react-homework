import * as React from 'react';

class Preloader extends React.Component {
  render() {
    return <div className="preloader"></div>;
  }
}

export default Preloader;
